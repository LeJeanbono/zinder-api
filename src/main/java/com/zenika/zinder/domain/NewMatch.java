package com.zenika.zinder.domain;

public class NewMatch {

    private Boolean match;

    public Boolean getMatch() {
        return match;
    }

    public void setMatch(Boolean match) {
        this.match = match;
    }

}
