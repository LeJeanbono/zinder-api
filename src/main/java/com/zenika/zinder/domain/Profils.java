package com.zenika.zinder.domain;

import java.util.List;

public class Profils {

    private List<Profil> profils;

    private Integer nbProfils;


    public List<Profil> getProfils() {
        return profils;
    }

    public void setProfils(List<Profil> profils) {
        this.profils = profils;
    }

    public Integer getNbProfils() {
        return nbProfils;
    }

    public void setNbProfils(Integer nbProfils) {
        this.nbProfils = nbProfils;
    }
}
