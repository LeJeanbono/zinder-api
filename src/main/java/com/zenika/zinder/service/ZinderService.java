package com.zenika.zinder.service;

import com.zenika.zinder.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class ZinderService {

    private Map<String, Profil> profils = new HashMap<>();
    private Map<String, Interet> interets = new HashMap<>();
    private List<Match> matchs = new ArrayList<>();

    public Profil saveProfil(NewProfil body) {
        Profil profil = new Profil();
        profil.setId(UUID.randomUUID().toString());
        profil.setNom(body.getNom());
        profil.setPrenom(body.getPrenom());
        profil.setPhotoUrl(body.getPhotoUrl());
        profil.setInterets(body.getInterets());
        profils.put(profil.getId(), profil);
        return profil;
    }

    public List<Profil> getAllProfils() {
        return List.copyOf(profils.values());
    }

    public List<Interet> getAllInterets() {
        return List.copyOf(interets.values());
    }

    public Match saveMatch(String idProfil, NewMatch body) {
        if (!profils.containsKey(idProfil)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Profil inconnu");
        }
        Match match = new Match();
        match.setId(UUID.randomUUID().toString());
        match.setProfil(idProfil);
        match.setMatch(body.getMatch());
        matchs.add(match);
        return match;
    }

    public List<Match> getAllMatchs() {
        return matchs;
    }

    public void deleteMatch(String idMatch) {
        matchs.removeIf(match -> match.getId().equals(idMatch));
    }

    @PostConstruct
    public void insertDatas() {
        String interet1 = UUID.randomUUID().toString();
        interets.put(interet1, new Interet(interet1, "Voyage"));
        String interet2 = UUID.randomUUID().toString();
        interets.put(interet2, new Interet(interet2, "Informatique"));
        String interet3 = UUID.randomUUID().toString();
        interets.put(interet3, new Interet(interet3, "Danse"));
        String interet4 = UUID.randomUUID().toString();
        interets.put(interet4, new Interet(interet4, "Poterie"));
        String interet5 = UUID.randomUUID().toString();
        interets.put(interet5, new Interet(interet5, "Photographie"));

        saveProfil(new NewProfil(
                "DiCaprio",
                "Leonardo",
                "https://vignette.wikia.nocookie.net/wikidoublage/images/b/b9/DiCaprio.jpg/revision/latest/scale-to-width-down/340?cb=20110317145342&path-prefix=fr",
                List.of(interet3, interet5, interet1)
        ));
        saveProfil(new NewProfil(
                "Bellucci",
                "Monica",
                "https://resize-parismatch.lanmedia.fr/img/var/news/storage/images/paris-match/people-a-z/monica-bellucci/en-images/5967888-1-fre-FR/Monica-Bellucci.jpg",
                List.of(interet4, interet3)
        ));
        saveProfil(new NewProfil(
                "Macron",
                "Emmanuel",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Emmanuel_Macron_in_Tallinn_Digital_Summit._Welcome_dinner_hosted_by_HE_Donald_Tusk._Handshake_%2836669381364%29_%28cropped_2%29.jpg/220px-Emmanuel_Macron_in_Tallinn_Digital_Summit._Welcome_dinner_hosted_by_HE_Donald_Tusk._Handshake_%2836669381364%29_%28cropped_2%29.jpg",
                List.of(interet4, interet2)
        ));
        saveProfil(new NewProfil(
                "Mathieu",
                "Mireille",
                "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwkQDgsJCQkNCwsNEA0JCAcOCA8ICQkNFREWFhUdExMYHSggGBonGxMTITEhJSkrLi4uFx8zODM4NygtLi4BCgoKDg0OFRAQFS0ZFRktKysrKysrKy0rKysrKystLSsyKysrKysrKy0rLS03ListLS0tLS0rNy0rKysrLTcrK//AABEIAOwAvAMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAAAAQIEBQYHAwj/xABGEAACAQIDBQUEBQkHAwUAAAABAgMABAUREiEiMTJCBgcTQVEjUmJyFDNhgpJDcZGhorLR0vAVJDSBseHiU8HCFkRjk5T/xAAaAQADAQEBAQAAAAAAAAAAAAAAAQIDBAUG/8QAKREAAgICAQMCBQUAAAAAAAAAAAECEQMhMQQSQRNRBSIycbEUI0Jhgf/aAAwDAQACEQMRAD8A2/OhRUKkA6GYoqFAB0KKhQAqhXKWaNQWd1UDqLVG3faXCYmCXF/DGx6Wl0UAS9ETUPH2nwhuTEYH+WWoLHu8vArQmN7gSyD8nGvjafmy4UBZdc6GdZM3fdhu3TYzMM93NtGqnll3z4G5CzQzw+8xXXpophZpuYoZ1X8F7Y4Jd5C0v42c/kWfwpfwtU7nQAuhSQ1KBFAAoUKFAwUKFCgAUKFCgAUKFCgBOdDOhQpAChQpvfXkUMck8zqiIrO0jNpVdNMR0mlRFMkjKiKNTSM2lVrLe2Xe/bQNJa4PCt1Ip0m9d/7qrfCBzVSe8XvFuL5ns7J2isVOlpBuNd/yrWeE+ZqlEVlgxrtrjl2SbvEJCh5YEbwoF+6tQbzSNmZJGbP1auJJ4mgPtq0IcwXcibUYr+akvM7EuxzJ3tq81NyaUGO3KgAFvTMUYz40RyO0cR+1RBqAO0UzqQyMysN7UrctXnsn3oY1aFYpZPptsP8A20ze1X5X8qoJHAg7D+zQDHjxy9KVBZ6o7JducKxFQLeTwrjL2lhK2iVfl96rPnXj+xvJEZZI3aKRTqSZW0aa2fsL3nM3h2WMtmxG5iP838azaour2jWgaOuMciMA0bBlI3WDc1dA3lQIVQoA0KABQoUKABQoUKACoUVHSAQ7qoZ3IVVGpmLcq1gPev25e7kfDrORlsozplZW/wAS3xfDVz73O13gQnDbSQeNMNM+TcsdYLMTmATnlvM3vVUUBxJ4kjL4aQT60qUngfPeolU5E+mn9qtCRJNAn0pTxuMsxlmKNYWIJGWQHvUAIY8KAJ4UtojkCNuylRWznIgH4l92gDkAfLjSivnlw3flqZhwaRshGoMgHieGV3Zl+Gkthrb6sDGw3fDbposdMiFz4eu7QI8xtFSM2EzqQCpyYak+KmrW0gbQylWz81pWFM4qTtA2ipOwmOwaiDnusOmm0Vq+YKjb1Zry06NrIhDFTkebJeWk9lRVGodhO3slsVssRctbZ6Y5+Z7bV+8v+lbHBNG6rJG6ujDUkitrVlrzPNECiyAbeWVh1VZew3bS4sZRY3LtLaNvJm31fy1A5LyjeVbypdMbG7imjjuIHDxsPEVg3NTxTQSKoUKFAAoUKFACAaj8exOO1t5LqU7qjl96n5NZb3vYztiwyN9uXiz5dPuj900hmU9or+W6u5ZpG1OX3c25dVR1nbeI5UKTqby91a7ww70gbawP4t2n2FQ+G0dw2wM3nv1Yyu3EDanBXLI6a7QQP7SMoSWCt8umrHPhSmUysM1YySNl1V3a2hh9pIysyJ5NvMvLTsmiIgwwvGxU5kq2nP4W0t/XxV2wzA31RlwChPhy/CzUT4rGuuOPIBmZlyb3qZnGp9TANpHNsbm08tMZM3OHWa5IzKjKNP8AXvUiOwg1B0KjT+z7wquy30khJdiXVNPzLXJL+ZQQGOkj8NMRanxMxMyvbqxU76ldCt9q01usatWOrSzKR5/Ww/8AGoAX8+evxCSRp29S1xO9mxGRpAWaHHY0Ko6LJHnrjYry08hexncOSFU+02/D5VS2DbAfKlLKy5ZMRSodl3lfD9JlQKWRtLqOqmlxfwO7BBuELJp93T/X7VVeKSTS2ROTN+7S4WYcSQCN74qKQWWhJOAQHwyF1b3w6q5YzBkiSKMip1ah7rUxw+fVm5HE6dvTU7cgNCNakFU06tPNUF2WPus7ZNHIMOun9m59lm3VW1QyDYykEEatQ6q8jGWSKUMpI0ndYNy16D7se0Zu7YRSyBpYx73MtDIZfQaFIRvI0ugQKFChQBykYAMxOQA1ba89drr/AMe/nmkYsS6rGo6V1fwWt17Q3AjtZ5DwCMu2vOM0yySiRW47z/8A2Mv/AJLQho5mEpcSKDkrjSmfxbv+tPLplSFVAyVh4i5ty6uam+IEDwpAuekeFLl8X+9Mbq6bSYyQyg8vu1QMdJjR2AEFlXUvxVEX9+z55Ege6V5abum9muZB9aWINgYjMZ+fTTugSs4OW2Plw3moGIbSDmcuX3qlrTCbmbJrW0lkAOnUqbtWbAO77FJisklq0MROn2m5/vUuaRaxtlCVDtIOTCgIjwzH4dFbND3SRl5DJdFY2OpNMe8tWLBO7jA7ZxO0LXMo5fG31X7vCp9Qfpnn1LGQ5FY2I+xKlsK7PXkjBEtZirbqyeE2n8Vek1w+0UBVtYVX3RAtdwigAKqgDpC1Lm2UopHnC/7IYqmoLYzSIh0+IsTPUNLhsyEiWCRWB1aTE27XqZgOBNR17Phib1w8Iy95alZGh9qZ5omgmUangdQo3WKMmmms7NsORAA3fir0PeXWAXqtZpJDIzjw/D06NVZh2r7EPBHK8DFkh3kz6o/9quOTeyXDWis2MwRQrHgGkbKphbktCWJAzXdUNy1WBmAIxtJOpqlbVjkIzwG9s6f63a0MxjiJXxCUBAI0tVz7o8V8G/iiL5JMfD2t1NVNucixIyOZ06RTrs3ctFdwzDZodZPloYHq1Tw9a7imsMiuiSLtDKrfipyp4VJIqhQoUgKj3kXGjD5xq06hXniF21ArqJA3l1c0bVt/fJdabBoVI1sVb5d6sLQ6pIhnkWTw/vdNNFLgkpHGQRipLD6s8skbVHyw5EsN4A6fG/7SfFXYrqBG05HSyrzQt1VyzkXfRlOzS299ZVWOhcFi8hWOKPax0qoj16Wb3a1vsz2Ew+FALuP6TIw9rr5fw1B932GRuq3ksajSfZ5VP472hvlkNjhUJlmbdlmC/U/L8Vc8pNujZKi6WVlbxIIreJYo15Y1TQtPAV4Aisr/ALA7VSKZJLpQW6Wn9qtJiwPtVHmFUyZHV4i4hzUh0auGFHqFUGwu+08ORubNpVHMobX+0tW6wvRIiuVZGI3o2XeWixUPyT5U0xK9SFGkfblyqOqnRNQuK2L3LiKRikC70rBt5vsX3aGH3KbiXabE7h/o2GRyOwOl2iTdX71Itex7t7XHrxtTHUtskvL8zVN47LeIktngUKQiJPFnmRF3f+VZXax3VzcwQzXF1JKx8O81voVfadLZ8uVNLQNmgTdmMFyD2sjJIOSYSa6eWokaN7O89oQunxD+Wjqk4zhdxbXCxYXiEtxEx0tCz62j+zUtaBg0FwYYzcR6JAOUtUyVeS1tcGKdqcPNlevAilomHiwM1MIHk2kcWPn01d+9mw37W6Y6VBaJ2C8vVVOszHqUHJY1HiNq6vmrpg7imc8lUmdGt5NqZZkjdyXlrhZhlljUkhnZYtI6d6pNb5SxCKS77vxN/X8KjrhwJ4w+3Jl8X4f6FUTZ6i7NyFrO1Ztp8NV1Hq01Loaieza5WdmD5Rx6fl07tS0ZqCELoUKFIZkHfHeKWktcs3CQt8vVWRiIlc0zLxnXpHu1p/etm2JzxnNcrWN1b3m01mKl9kkZydT5dVNFrgRdXMoIuIjmxHtV/wCpSHxpWz1QqGPUKdyqCC7RhVbqXlVqjbqwba0bK45tnNV6fItrg2XsI39wt2HUviVJWZjgWa7l0rJIzOzHpWoHu3nDWESeaM0fy1Zr/BXuF8PxDEpGnUFrjl9Ts6lxZWZu0OK3jXMWEFY44EZp7srrbl4L8VVuwxLGXmiikxK8km8XS8YdvA8PpOrPmz1eVX/Cuxk9rrFrdbGGrSU6qnLLs9lq8RghY6m8JFi1fMy1paWkTXlsEMt3A6RZtdRMOYL7Vfm8mqZKjMOBlnSre0jjGUa5bdW1uajY+XCpE6OynZXAAZsD50uM+XpSGO9nQJjM4Qg1GB2jLHebVr1U1fs8jadbsFA3lXTFqb13am1bypdFFdzIO1wC0i3ljVnO94hXeanToNoGz81P2FMpgduVJlJ2Zv3sW4a1RgMwsis2VZNMxyyAABG9l1VtPeNHqspt3Mje+WsSErZkAgbfOt8fBllWx7Za0zlAPindizXl+2uF4pRs3JLseU+9XezbaZX1Mi7zMeqmVzM8spkYAM50qo5VWtTF6PTXdtiX0jC7OQtqZU8Jm97Tu1alNZB3B4i+i9wx2zEZW5i3uVW5v11r1ZvklHUGjpANKzoGY53z27R3lteIARJBp+Vkb+FZIki6smJUHdZvdb1r0P3t4K1zh0lxECZrM/SdIH1kPWP0bfu15wLjNiNoJppWikyfSJGUxs2UgHL0zf8AKo6W1HtFU5OPqozRWd5kNEi6ly1Iw5l+WncxDgMWDg/lAu996iiuS2d1M3+Jt22MrrJp+atWtwdg+ysF7H3xtb1WMmcUh8N863WxmDKrg5gisMi+Y3xu4ktFlsNOAR5U1Q8K7qf1UkEhZptMRxNOM/M0zmYZszEKqjVt6aGyUdYj/nSZgdpyyphhuNWUrmOKQlgdKq0TRavl1c1OMVvXRcoo/Elb6qENoVvmbpWleiqadCkcgjPYKeq3Coeye+ZVF3FErE7zRtur+LbUgrUkwaO703mHGupb0rlMdhqmCKv2kgR4pBK6pGo8SWR23VVd7erCb6BHmnniCrC0jPFm2jd6d3jWo98ZkFlCFYqGnXxMm5l8NqySN0XaGJOXlWuNaszySvR2KvpOokDPdjC6NVc2i0sq5bcqcWoOayynIA+zX3qKTed5CfPSuVamXJpHcQrG/vmAzVYt77zVuIFed+7vtU2G/SXSxWd5xGupp/C0afu1ZcQ7y8ZkKm2WK1QDlCfSGb7xrGWSKZ1Y+hzZdxWjYwKVkawWftlj75hsTlUfAqxfuim3/qTGBs/tS6//AFSVn+oXsdS+D5fMkegpUVlaNgCGDKynq1V5Fxi2MF5cwBchFLNF4fwq1eva8598eASW+JzXaRkQXZ8eJgu74mneH6dtdEWeSU+18PgSCpO43u08ngGwaWjc8sq8slQgYjMLwz1fLTmC/mXJc9Sqd1S3LTaLT9xzJLpIJYa1OrbzVsHd3jK3Fqqls5YT4Tr+7WPSzQybZA0bn1TdarL3b4kIL5YhIDHcDwtOr8p0/wAKzmrRrB0zdYD5+tOFplCTsI4U4V6xLezqWFN5Ykfay5/e5qN3ABJOQG9tqOfF4RmIw0rfDy/iqSoxb4HzWkTZDSARvKwXlrrHbgbSSxA86hJcTumGcapGPe564XF5fFQjtsO9s06mWno1WCb5ZYiV4Aj8VET51WVsrhgXETkDqLeEq/iqD7SpiMWUUEixysyrqEuvwVbz0/mpWN4Y3Xds0FX9ONHMNhNMcAgdbaBZXeVympppG1M3zU/n8xTOeWnRlvfTIPo1pFmAXm1bfhjb+asmhRdpYHIfDzVfu+O6Z7u1tFzKQxtLLl0s7fwWqXBZJsylYg82fTXTDUTBpuRyDk5AEKvLUhBDEoDSE5Abq+9RNDCm1dhPUW3m+Wlwozn0WlOaSN8HTynKh1ZpxYjaTqp+FHlXKFMshXWuCTtn1GCHbFIG3z4UMjShlQyFI2o9LVD9p8Atb+2ks7gAFhqgnC78EvSy1L0VegfDHlDtR2cvbG4ks7uMq6nVFKF9lcx+q1Egocll9mwGnxB1fNXrPHsDsL6Fra/t1kQjcYr7WJvVW6TWP493OXyOz4dMl1FvaVk9lOq+nxNVWUmjNlt5MgVYOnSwakLNPE0c4bw2RlePNdGll3qt8PdrfrHcXV7KmH29urPPK6NKzafKNepjwqoPhjbWZmY5+dS5Jcm8Mc5r5FdHovsrisV3aW95Ec1kTeX3W6h+KphR5VivdXjjWkzYdcNlbzspiYt9VL/Kf9a2pDwYGud1einGUfqGuMYeJ4Jbcsy6husj+Eyt8y1n2GJdW8hS7kkvIBJvLr0Txx8rBl6vXjWnORlVfxjCFlJliIjmHUeWT5qTN8M1xIVYYjhujSbTw5Ne6smnej1bp1N9lPv7Ys10ukIBOrqj3fw1W1tbxMhJasxHUq+KrU8gW92iOBlDf/AqN+JqEzWXTwe+6/8AQsVxa5kWRCoihkXw54GTxWbp3WqFtraSeZYgWYHS08zNrZY195m/QKnWwW7kIaWRY16mO/LUtY2EUK6I1yz52O+zN8VIrvx441HkeQlRkg2BRp+Wk3kgCs7HIAaq5Fhnxqv9t8RKWc6RtlIyadnTqp2ccY90kjIu0Rkuby5vA50u+lFPKsa7q/qqIFpOTkWyIPkvLU2iDZ6UpUG3MfFU+oz2v0GNkZBhx2F2LH7d+pKGEDIAZV1CilVDk2dmLp4QWkAChlQoVJ0UChQoUAelcqKjNFXoHwgKGVCo7HcatLKFrq7fSo2JGPrJW9FWh6HGLk6XJQu93FszBhMTZAD6VdKG5vcH7x/RWYOg2jLjUrjWJPdXNxfSLpMzZqmr6tekfoqPArinK5WfWdN0yxYVH+XkZNCPzEjy6a1PsJ2n8SCO3vH9onslmP5TT73xVmzLtFSfZx1E30eTYk26re7IvLQmYdX06lC/Y2xZVPA5g1yIHDOqhb315BkhbxYx73TUta4zG5AbNCfWr7rPIeNrgnAorquXA02hmUgbQacoR5GqRDFso9K4v55V2LetML25jQEswAA1U2KOyOxS9Ee3PNjuqo6qqfaJ5Gtp3c6iyfhqSupXkk8RhkoPs1rhiEAeN0IzDK1ZM6cfytGfKOFKI4GjUHgeIoyKzPpIq0AUKSD5etKGVBSdgoUKFAwUKFHQB6UoqFRXaDHrOyiM90+07sNuv1s7ei13t0fDRi5OorYfaLHLWyga5uWzz3IIB9bPJ6LWHY9jN3eztc3T5kfVQD6qBPRaX2hxu6vZ2urk5AbsFsG9lAnov8ajQK5cmTu0uD6XoehWJd0vr/ACKJfOlGkisj0/IUg4H0NDJtjI2TKdSMOllpTjjRrQJxTbTNAwW/juIY5HA15aZVHS3VTw2ycAMqpHZy+8GYxk7kh/C1XuCVSAc+NM8HqMTxTaCiknj+rbMDpNOocXm4GNc/mpJUceINGkA4gVSs5tPkU+IXTZgMEB9Fpq0LsdUjsx+1qfJEPMAUbRjyp03yTaXBFMgzIHlXWCEMpJHE0u4TLM5U7tYToAy25UwvRlmKQeHcTx8AHZl+Vqa1MdromW7kJXSGC6cl5qh6yZ9L00u7FF/wBBEeVIB8q6GkkUGrDoUmlUAgUNYGygSdgHE0BkNlAc8G49ru1ltYRgECW6cf3ezDfrf3VrGcVxO6upXu7yQySMdK+7Gvoq9K03u7maaR7idzJLIc3lZuNJA8h5VpObkcHRdDDAr5l7hZcaMChR1meggqTSjRUAwyKKP0o6IUA+Qz6jZl6dNXHAL8yRKS2bp7N6p9OMPvHhlWVcyPysf/UWhaOfqsPqw1yaNDNwz4U+gkGwZ8airbJ1SWM5ow1Kw6qfwKdgPGtD56SrQ9JH+VESOAosuFdIk8/SqMzhPFmCcvOnVuNgzHlXSSIaCPOlwgAZeYFAeCidvLF/Chu2GbLI0bNq6W5apdaj2yhX+z5FHBAsi57/AFVl1ZSPe+HSvDXswqKjzos/Wkd4mjJHE0CRtLEAAattckYtv5EKOXOglutHRR5nif2aVSaVQNAA4Uo0XpQoGgUKFCgYDRGjojQJhik0YoEUB4DBoEUAaFAy89314siyWEjDUg8W2z6o+of5H96rWbQjaM9lZJhd9Jbzw3kW1o21afeXqH6K2i0uYpoormBtUci60arjweD1+Fwydy4f5OMcezb5V1hXjmKUFHnS1G3Z51Z57DyG37a5OBkSRmB+1XciuMueagAnM+TctJgMMfg12dzHpz9m2nKsbBrbr58oZd3MhGbTz6t2sR1DaRtOflUyPX+GPUkCkvIi5Fm47qqOZqVt/NXPwkzMgXMn1qD1HfgQVZ8jJsA5Y66rlwHlQoUCSrYqhSaVQUhQHDL0oUY4/wCVF/GgoKhRmiNAgGiNHRUAChQoUAgUdFRigAGrb2Bx2SKQ2Eh1QNqlXNt6Fl5tP+uVVKlWs7xyJNEdLo6PGw8iKE6MOoxLJjaZuMbIwDowZT1Bq6ZcKh55DEsM8OSmYeLNFl7IsV4hfKpUMcs/OtT5liya4KxJLnIqRuZLvUi6kYBcjl7SNM/srogGweVABOMwQeB3ax7tFAsd5cxoMlD6l+9WyFR+qsj7X/4+5/Ov7tTI9L4a/wBx/Yh6MUVCoPbAR6UkilGiNAmggaFFR0Eo/9k=",
                List.of(interet5, interet1, interet2)
        ));
    }
}
