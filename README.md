# Zinder
L'équipe back vient de terminer l'API Zinder.\
Le produit doit sortir aujourd'hui pour la St Valentin.\
A vous de construire le front demandé par le client.\
Le PO a rédigé quelques US...

PS : l'API démarre su le port 8088\
PS2 : Les données sont effacées à chaque redémarrage de l'API.\
PS3 : Pas de documentation faite par l'équipe, à vous d'explorer le controller de l'API.\
PS4 : Vous êtes libre d'utiliser n'importe quelle librairie, script dans votre application.\
PS5 : Chaque encadré bleu sur les maquettes est un composant.\
PS6 : Bonne courage !


## US 1 : Implémentation de la maquette
En tant que qu'utilisateur, je souhaite avoir sur mon site le design défini par la maquette.\
Afin de respecter la charte graphique de mon entreprise.
![Drag Racing](Zinder.JPG)


## US 2 : Liste des profils
En tant qu'utilisateur, je souhaite avoir la liste des profils avec leur prénom, leur nom et leur photo.\
Afin de pouvoir les matcher.

## US 3 : Match d'un profil
En tant qu'utilisateur, je souhaite pouvoir matcher un profil avec un clic sur 'Like' ou sur 'Nope'.\
Afin de pouvoir retrouver le profil plus tard.

## US 4 : Page de statistiques
En tant qu'utilisateur, je souhaite pouvoir accéder à une page de statistiques via l'url '/stats' grâce au lien 'Statistiques' dans le header.

## US 5 : Liste des matchs
En tant qu'utilisateur, je souhaite pouvoir visualiser la liste des matchs que j'ai fait, qu'ils soient 'Like' ou 'Nope'.\
Afin de pouvoir retrouver des anciens matchs.\
![Drag Racing](Zinder2.JPG)

## US 6 : Liste des intérêts
En tant qu'utilisateur, je souhaite voir la liste des intérêts d'un profil.\
Afin de pouvoir choisir quelqu'un qui posséde les mêmes que les miens.\
![Drag Racing](Zinder3.JPG)

## US 7 : Filtre des profils spar intérêt
En tant qu'utilisateur, je souhaite pouvoir filtrer les profils selon un intérêt choisi dans une liste déroulante.\
Afin de pouvoir retrouver plus rapidement quelqu'un qui posséde les mêmes intérêts que les miens.

## US 8 : Supprimer un match
En tant qu'utilisateur, je souhaite pouvoir supprimer un de mes matchs de la liste en cliquant sur un bouton supprimer.\
Afin de pouvoir annuler un 'Nope' ou un 'Like' dont je ne veux plus.

## US 9 : Page d'admin
En tant qu'utilisateur, je souhaite pouvoir accéder à une page d'administration via l'url '/admin' grâce au lien 'Administration' dans le header.

## US 10 : Création d'un profil
En tant qu'utilisateur, je souhaite pouvoir ajouter un profil à la liste des profils en allant sur la page d'administration.\
Je dois renseigner un prénom, un nom, une url de photo en ligne et une liste d'intérêts.\
Afin de pouvoir inscrire d'autres personnes.

## US 11 : Graphique des matchs (Bonus)
En tant qu'utilisateur, je souhaite voir un camenbert de répartition des matchs.\
Afin de me rendre compte des proportions de 'Like' et 'Nope'.
Note : voici une aide pour les graphiques : https://medium.com/codingthesmartway-com-blog/angular-chart-js-with-ng2-charts-e21c8262777f

## US 12 : Icones (Bonus)
En tant qu'utilisateur, je souhaite afficher des icones pertinantes à la place de 'Like', 'Nope' et 'Supprimer'.\
Afin de rendre mon application plus attractive.

## US 13 : Non affichage des profils déjà matché (Bonus)
En tant qu'utilisateur, je souhaite que les profils ayant déjà été matché ne me soit plus proposé.\
Afin de je pas rematcher sur ces profils